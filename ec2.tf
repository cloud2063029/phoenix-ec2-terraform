resource "aws_security_group" "ec2_public_security_group" {
  name = "Public EC2 security groups"
  vpc_id = module.vpc.vpc_id

  ingress {
    from_port = 80
    protocol = "TCP"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

/*  #ssh access
  ingress {
    from_port = 22
    protocol = "TCP"
    to_port = 22
    cidr_blocks = ["0.0.0.0/0"]
  }*/

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "ec2_private_security_group" {
  name = "Private EC2 security groups"
  vpc_id = module.vpc.vpc_id

  ingress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = [aws_security_group.ec2_public_security_group.id]
  }

  //health check
  ingress {
    from_port = 80
    protocol = "TCP"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "load_balancer_sg" {
  name = "ELB security groups"
  vpc_id = module.vpc.vpc_id

  ingress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

data "aws_iam_policy_document" "ec2_assume_role_iam_policy" {
  statement {
    sid = "EC2_AssumeRole"
    effect = "Allow"
    resources = ["*"]

    principals {
      type = "Service"
      identifiers = [
        "ec2.amazonaws.com",
        "application-autoscaling.amazonaws.com"
      ]
    }

  actions = ["sts:AssumeRole"]
  }
}

data "aws_iam_policy_document" "ec2_iam_role_policy" {
  statement {
    sid = "EC2_Role"
    effect = "Allow"
    resources = ["*"]

    actions = [
      "ec2:*",
      "elasticloadbalancing:*",
      "cloudwatch:*",
      "logs:*"]
  }

}

//will be created by ec2 module
/*module "EC2_role" {
  source = "cloudposse/iam-role/aws"
  enabled = true
  role_description = "EC2_role"
  policy_documents = [
    data.aws_iam_policy_document.ec2_assume_role_iam_policy.json,
    data.aws_iam_policy_document.ec2_iam_role_policy.json
  ]

}*/

data "aws_ami" "ami" {
  most_recent = true

  filter {
    name   = "owner-alias"
    values = ["amazon"]
  }
}

module "ec2s" {
  source = "terraform-aws-modules/ec2-instance/aws"

  for_each = toset(["one", "two", "three"])
  name = "instance-${each.key}"

  ami = data.aws_ami.ami.name

  iam_role_policies = {
    ECS2 = data.aws_iam_policy_document.ec2_iam_role_policy.json,
    EC2AssumePolicy = data.aws_iam_policy_document.ec2_assume_role_iam_policy.json,
  }
  iam_role_name = "EC2_role"

  create_iam_instance_profile = true


  instance_type          = "t2.micro"
  key_name               = "user1"
  monitoring             = true
  vpc_security_group_ids = ["sg-12345678"]
  subnet_id              = "subnet-eddcdzz4"

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }

}
